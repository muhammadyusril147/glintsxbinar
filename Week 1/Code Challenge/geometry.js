function tube(phi, radius) {
  console.log("Tube's Volume : " + phi * radius ** 2);
  return phi * radius ** 2;
}
function cube(side) {
  console.log("Cube's Volume : " + side ** 3);
  return side ** 3;
}
const phi = 3.14;
let VolumeOfTube = tube(phi, 5);
let VolumeofCube = cube(3);
console.log(
  "Volume of Tube + Volume of Cube :" + (VolumeOfTube + VolumeofCube)
);
