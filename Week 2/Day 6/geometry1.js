const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});
function tube(phi, radius) {
  return phi * radius ** 2
}
const phi = 3.14
function input() {
    rl.question('Radius: ', (radius) => {
        if (!isNaN(radius)) {
            console.log(`\ntube's Volume: ${tube(phi, radius)}`);
            rl.close();
        } else {
            console.log(`radius must be a number\n`);
            input();
        }
    })
}

console.log(`TUBE`);
console.log(`=====`);
input()