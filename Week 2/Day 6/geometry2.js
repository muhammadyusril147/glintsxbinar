const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
function cube(side) {
  return side ** 3;
}

function input() {
  rl.question("Side: ", (side) => {
    if (!isNaN(side)) {
      console.log(`\ncube's Volume: ${cube(side)}`);
      rl.close();
    } else {
      console.log(`side must be a number\n`);
      input();
    }
  });
}

console.log(`CUBE`);
console.log(`=====`);
input();
