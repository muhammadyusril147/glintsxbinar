let people = [
  {
    name: "yusril",
    status: "negative",
  },
  {
    name: "aqilla",
    status: "positive",
  },
  {
    name: "aat",
    status: "suspect",
  },
  {
    name: "candra",
    status: "negative",
  },
  {
    name: "adi",
    status: "positive",
  },
  {
    name: "ray",
    status: "suspect",
  },
  {
    name: "rizal",
    status: "negative",
  },
  {
    name: "bangkit",
    status: "positive",
  },
  {
    name: "arya",
    status: "suspect",
  },
];

let userChoice = 2;

switch (userChoice) {
  case 1:
    let negativePeople = people.filter(
      (person) => person.status === "negative"
    );
    console.log(negativePeople);
    break;
  case 2:
    let positivePeople = people.filter(
      (person) => person.status === "positive"
    );
    console.log(positivePeople);
    break;
  case 3:
    let suspectPeople = people.filter((person) => person.status === "suspect");
    console.log(suspectPeople);
    break;
  default:
    console.log("none");
}

exports.people = people;
