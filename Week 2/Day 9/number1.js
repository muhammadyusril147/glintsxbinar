const fruits = ["tomato", "broccoli", "kale", "cabbage", "apple"];

for (let i = 0; i < fruits.length - 1; i++)
  console.log(`${fruits[i]} is a healthy food, it's definitely worth to eat`);
