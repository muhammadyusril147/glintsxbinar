const data = require("./arrayFactory.js");
const test = require("./test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}
console.log(clean(data));

// Should return array
function sortAscending(data) {
  // Code Here
  //   console.log(clean(data));
  //   data.filter((i) => typeof i === "number");
  const len = data.length;
  for (let i = 0; i < len - 1; i++) {
    for (let j = 0; j < len - 1; j++) {
      if (data[j] > data[j + 1]) {
        const temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }

  return data;
}
const filteredData = data.filter((i) => typeof i === "number");
console.log(sortAscending(filteredData));

// Should return array
function sortDecending(data) {
  //   Code Here
  //   data.filter((i) => typeof i === "number");
  const len = data.length;
  for (let i = 0; i < len - 1; i++) {
    for (let j = 0; j < len - 1; j++) {
      if (data[j] < data[j + 1]) {
        const temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }

  return data;
}
console.log(sortDecending(filteredData));

// DON'T CHANGE
test(sortAscending, sortDecending, data);
