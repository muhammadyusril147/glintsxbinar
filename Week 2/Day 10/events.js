const eventEmitter = require("events");
const readline = require("readline");

const my = new eventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

my.on("login:failed", function (email) {
  console.log(`${email} is failed to login`);

  rl.close();
});
my.on("login:succsess", function (email) {
  console.log(`${email} is success to login`);
  console.log("now you have access to the data 'positive'");
  const { people } = require("../Day 9/number2");
  rl.close();
});

function login(email, password) {
  const passwordInDatabase = "123456";
  if (password !== passwordInDatabase) {
    my.emit("login:failed", email);
  } else {
    my.emit("login:succsess", email);
    // console.log("now you have access to the data");
    // const { people } = require("../Day 9/number2");
  }
}

rl.question("Email: ", (email) => {
  rl.question("Password: ", (password) => {
    login(email, password);
  });
});
