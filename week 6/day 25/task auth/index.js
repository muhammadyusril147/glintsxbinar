require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });
const express = require("express");

const router = require("./routes/index");

const errorHandler = require("./middlewares/errorHandler");

const port = process.env.PORT || 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", router);

app.all("*", (req, res, next) => {
  next({ statusCode: 404, message: "endpoint is not found" });
});

app.use(errorHandler);

app.listen(port, () => console.log(`server running on ${port}`));
