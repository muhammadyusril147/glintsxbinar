const router = require("express").Router();
const UsersController = require("../controllers/users");

router.get("/", UsersController.getAll);

module.exports = router;
