const router = require("express").Router();
const TasksController = require("../controllers/tasks");
const { isSignedIn } = require("../middlewares/auth");

router.get("/", isSignedIn, TasksController.getAll);
// router.get("/:id", TeachersController.getTeacherById)
router.post("/", isSignedIn, TasksController.create);

module.exports = router;
