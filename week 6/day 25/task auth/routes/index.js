const router = require("express").Router();
const usersRoute = require("./users");
const tasksRoute = require("./tasks");

const SignInController = require("../controllers/signIn");
const UsersController = require("../controllers/users");

router.use("/users", usersRoute);
router.use("/tasks", tasksRoute);

router.post("/signin", SignInController.signIn);
router.post("/signup", UsersController.create);

module.exports = router;
