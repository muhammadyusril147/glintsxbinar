const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("task", taskSchema);
