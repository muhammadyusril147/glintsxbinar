const { User } = require("../models");
const encryption = require("../helpers/encryption");
const { generateToken, decodeToken } = require("../helpers/jwt");
const user = require("../models/user");

class SignInController {
  static async signIn(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;

    const data = await user.findOne({ email: email });
    if (!data) {
      return res
        .status(400)
        .json({ statusCode: 401, message: "email/password invalid" });
    }

    let isPassValid = encryption.isPassValid(password, data.password);

    if (!isPassValid) {
      return res
        .status(401)
        .json({ statusCode: 401, message: "email/password invalid" });
    }

    const { _id, task } = data;

    const payload = {
      _id,
      email,
      task,
    };

    const token = generateToken(payload);

    return res
      .status(200)
      .json({ statusCode: 200, message: "login success", token });
  }
}

module.exports = SignInController;
