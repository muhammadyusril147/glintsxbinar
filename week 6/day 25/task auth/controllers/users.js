const { user } = require("../models");
const Encryption = require("../helpers/encryption");

class UsersController {
  static async create(req, res, next) {
    try {
      const email = req.body.email;
      const task = req.body.task;
      const password = Encryption.encryptPass(req.body.password);
      const objUser = { email, task, password };
      const createdData = await user.create(objUser);

      let data = await user.find({ _id: createdData._id }).select("-password");

      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }
  static async getAll(req, res, next) {
    try {
      let token = req.headers.token;
      let userData = getUserData(token);
      let userTask = userData.task;
      const users = await user
        .findAll({ task: userTask })
        .populate({ path: "task" });
    } catch (error) {}
  }
}

module.exports = UsersController;
