const { task } = require("../models");

class TasksController {
  static async create(req, res) {
    if (req.userData.task != "618b7da688f2d345cff98fec") {
      return res.status(403).json({
        statusCode: 403,
        message: "you are not allowed to access this endpoint",
      });
    }
    const name = req.body.name;
    const objTask = { name };
    const createdTask = await task.create(objTask);
    if (createdTask) {
      return res.status(200).json(createdTask);
    }
  }

  static async getAll(req, res) {
    const tasks = await task.find({
      _id: req.userData.task,
    });
    res.status(200).json(tasks);
  }
}

module.exports = TasksController;
