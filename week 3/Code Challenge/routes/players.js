const express = require("express");

const {
  getAllPlayers,
  getDetailPlayers,
  addPlayers,
  updatePlayer,
  deletePlayer,
} = require("../controllers/players");

const router = express.Router();

router.get("/", getAllPlayers);

router.get("/:id", getDetailPlayers);

router.post("/", addPlayers);

router.put("/:id", updatePlayer);

router.delete("/:id", deletePlayer);

module.exports = router;
