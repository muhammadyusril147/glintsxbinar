const express = require("express");

const app = express();

const players = require("./routes/players");

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/players", players);

app.listen(port, () => {
  console.log(`Server running on ${port}!`);
});
