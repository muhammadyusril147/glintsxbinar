let data = require("../models/data.json");

class Player {
  getAllPlayers(req, res, next) {
    try {
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  getDetailPlayers(req, res, next) {
    try {
      let detailData = data.filter(
        (item) => item.id === parseInt(req.params.id)
      );

      if (detailData.length === 0) {
        return res.status(404).json({ errors: ["Player not found"] });
      }

      res.status(200).json({ data: detailData });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  addPlayers(req, res, next) {
    try {
      let lastId = data[data.length - 1].id;

      data.push({
        id: lastId + 1,
        number: req.body.number,
        name: req.body.name,
        nationality: req.body.nationality,
        position: req.body.position,
      });

      res.status(201).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  updatePlayer(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Student not found"] });
      }

      data = data.map((item) =>
        item.id === parseInt(req.params.id)
          ? {
              id: parseInt(req.params.id),
              number: req.body.number,
              name: req.body.name,
              nationality: req.body.nationality,
              position: req.body.position,
            }
          : item
      );

      res.status(201).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  deletePlayer(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Student not found"] });
      }

      data = data.filter((item) => item.id !== parseInt(req.params.id));

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }
}

module.exports = new Player();
