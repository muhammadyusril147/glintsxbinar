import axios from "axios";

let urlAuthor =
  "https://gist.githubusercontent.com/sahlannasution/680bd223b440c06604fb39565fd67a9d/raw/7f8577a0a06066340bdd282f6a28ed08575ac4be/getAuthorArticles.json";
let urlArticle =
  "https://gist.githubusercontent.com/sahlannasution/9fb3ce93520865911b267caf044a7c5b/raw/545e9d97c9dc69dff97df3fd188992f99f778b9b/getSubmittedArticles.json";
let urlPublisher =
  "https://gist.githubusercontent.com/andityadimas1/380de7c81ff8046b1045fc325abec4c1/raw/eb604cdcb036f295bf229117f906cb75bd392c5e/getUserPublisher.json";
let data = {};
const fetchApi = async () => {
  try {
    let response = await Promise.all([
      axios.get(urlAuthor),
      axios.get(urlArticle),
      axios.get(urlPublisher),
    ]);
    data = {
      author: response[0].data.data.getAuthorArticles,
      article: response[1].data.data.getSubmittedArticles,
      publisher: response[2].data.data.getUserPublisher,
    };
    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
