import fetch from "node-fetch";
const fetchApi = async () => {
  try {
    const author = await fetch(
      "https://gist.githubusercontent.com/sahlannasution/680bd223b440c06604fb39565fd67a9d/raw/7f8577a0a06066340bdd282f6a28ed08575ac4be/getAuthorArticles.json"
    );
    const article = await fetch(
      "https://gist.githubusercontent.com/sahlannasution/9fb3ce93520865911b267caf044a7c5b/raw/545e9d97c9dc69dff97df3fd188992f99f778b9b/getSubmittedArticles.json"
    );
    const publisher = await fetch(
      "https://gist.githubusercontent.com/andityadimas1/380de7c81ff8046b1045fc325abec4c1/raw/eb604cdcb036f295bf229117f906cb75bd392c5e/getUserPublisher.json"
    );

    const results = await Promise.all([
      author.json(),
      article.json(),
      publisher.json(),
    ]);
    console.log(results[0].data.getAuthorArticles);
    console.log(results[1].data.getSubmittedArticles);
    console.log(results[2].data.getUserPublisher);
  } catch (error) {
    console.error(error.message);
  }
};
fetchApi();
