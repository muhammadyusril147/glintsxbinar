import axios from "axios";

let urlAuthor =
  "https://gist.githubusercontent.com/sahlannasution/680bd223b440c06604fb39565fd67a9d/raw/7f8577a0a06066340bdd282f6a28ed08575ac4be/getAuthorArticles.json";
let urlArticle =
  "https://gist.githubusercontent.com/sahlannasution/9fb3ce93520865911b267caf044a7c5b/raw/545e9d97c9dc69dff97df3fd188992f99f778b9b/getSubmittedArticles.json";
let urlPublisher =
  "https://gist.githubusercontent.com/andityadimas1/380de7c81ff8046b1045fc325abec4c1/raw/eb604cdcb036f295bf229117f906cb75bd392c5e/getUserPublisher.json";
let data = {};

axios
  .get(urlAuthor)
  .then((response) => {
    data = {
      author: response.data.data.getAuthorArticles,
    };
    return axios.get(urlArticle);
  })
  .then((response) => {
    data = { ...data, article: response.data.data.getSubmittedArticles };

    return axios.get(urlPublisher);
  })
  .then((response) => {
    data = { ...data, publisher: response.data.data.getUserPublisher };
    console.log(data);
  })
  .catch((err) => {
    console.error(err.message);
  });
